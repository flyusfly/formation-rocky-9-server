#!/bin/bash
#
# setup.sh

COLOR_GREEN='\033[1;32m'
COLOR_RED='\033[0;31m'
COLOR_WHITE='\033[1;37m'
COLOR_NC='\033[0m'
TEXT_BOLD='\033[1m'
TEXT_NORMAL='\033[0m'
CWD=$(pwd)
SLEEP=1
SUPPORT_PRODUCT_EXPECTED="Rocky Linux"
SUPPORT_PRODUCT_VERSION_EXPECTED="9"
TOOLS=$(grep -E -v '(^\#)|(^\s+$)' ${CWD}/pkglists/tools.txt)
USERS=$(awk -F: '$3 > 999 && $3 < 65534 {print $1}' /etc/passwd | sort)

echo
echo -e "${COLOR_WHITE}.: ${SUPPORT_PRODUCT_EXPECTED} ${SUPPORT_PRODUCT_VERSION_EXPECTED} post-installation configuration :.${COLOR_NC}"
echo

# - Check OS name & OS Version for process
# - --------------------------------------
source /etc/os-release
MAJOR_VERSION=$(awk -F"." '{print $1}' <<< $REDHAT_SUPPORT_PRODUCT_VERSION)
if [ "${REDHAT_SUPPORT_PRODUCT}" != "${SUPPORT_PRODUCT_EXPECTED}" ] || [ "${MAJOR_VERSION}" != "${SUPPORT_PRODUCT_VERSION_EXPECTED}" ]
then
  echo "=> A ${SUPPORT_PRODUCT_EXPECTED} ${SUPPORT_PRODUCT_VERSION_EXPECTED}.xx Operating System is expected! Bye!"
  echo
  exit 1
fi

# - Check that user is root or has superuser privileges
# - ---------------------------------------------------
if [ "${UID}" -ne 0 ]
then
  echo
  echo -e "${COLOR_RED}  => You must have root privileges. Use sudo or be root! Bye!${COLOR_NC}"
  echo
  exit 1
fi

# - .bashrc
# - -------

echo -e "${COLOR_WHITE}. Installing custom .bashrc${COLOR_NC}"

echo "  .. Admin user: root"
cp -f ${CWD}/bash/bashrc-root /root/.bashrc
sleep ${SLEEP}

echo "  .. Existing users"
if [ ! -z "$USERS" ]
then
  for USER in ${USERS}
  do
    if [ -d /home/${USER} ]
    then
      echo "    ... Installing custom .bashrc for user: ${USER}"
      cp -f ${CWD}/bash/bashrc-users /home/${USER}/.bashrc
      chown ${USER}:${USER} /home/${USER}/.bashrc
      sleep ${SLEEP}
    fi
  done
fi

echo "  .. Future users"
cp -f ${CWD}/bash/bashrc-users /etc/skel/.bashrc
sleep ${SLEEP}

# - .vimrc
# - ------
echo
echo -e "${COLOR_WHITE}. Installing custom .vimrc${COLOR_NC}"

echo "  .. Admin user: root"
cp -f ${CWD}/vim/vimrc /root/.vimrc
sleep ${SLEEP}

echo "  .. Existing users"
if [ ! -z "$USERS" ]
then
  for USER in ${USERS}
  do
    if [ -d /home/${USER} ]
    then
      echo "    ... Installing custom .vimrc for user: ${USER}"
      cp -f ${CWD}/vim/vimrc /home/${USER}/.vimrc
      chown ${USER}:${USER} /home/${USER}/.vimrc
      sleep ${SLEEP}
    fi
  done
fi

echo "  .. Future users"
cp -f ${CWD}/vim/vimrc /etc/skel/.vimrc
sleep ${SLEEP}

# - SSH Server
# - ----------
echo
echo -e "${COLOR_WHITE}. Configuring SSH Server${COLOR_NC}"

echo "  .. Don't inherit system locale on SSH Server"
sed -i -e '/AcceptEnv/s/^#\?/#/' /etc/ssh/sshd_config
systemctl reload sshd
sleep ${SLEEP}

# - Persistent password for sudo
# - ----------------------------
echo
echo -e "${COLOR_WHITE}. Configuring persistent password for sudo${COLOR_NC}"

echo "  .. No password timeout for sudo"
cp -f ${CWD}/sudoers.d/persistent_password /etc/sudoers.d/
sleep ${SLEEP}

# - Rocky Linux Repositories
# - ------------------------
echo
echo -e "${COLOR_WHITE}. Configuring official Rocky Linux repositories${COLOR_NC}"

echo "  .. Removing existing repositories"
rm -f /etc/yum.repos.d/*.repo
rm -f /etc/yum.repos.d/*.rpmsave
sleep ${SLEEP}

echo "  .. Enabling Repositories"
for REPOSITORY in BaseOS AppStream Extras CRB
do
  echo "     ... Repository: ${REPOSITORY}"
  cp -f ${CWD}/dnf/Rocky-${REPOSITORY}.repo /etc/yum.repos.d/
  sleep ${SLEEP}
done

echo
echo -e "${COLOR_WHITE}. Enabling/Disabling repositories${COLOR_NC}"

echo
echo ". Enabling/Disabling repositories"

echo "  .. Enabling repository: EPEL"
if ! rpm -q epel-release > /dev/null 2>&1
then
  dnf install -y epel-release > /dev/null
fi
cp -f ${CWD}/dnf/epel.repo /etc/yum.repos.d/
sleep ${SLEEP}

echo "  .. Enabling repository: EPEL Modular"
cp -f ${CWD}/dnf/epel-modular.repo /etc/yum.repos.d/
sleep ${SLEEP}

echo "  .. Removing repository: EPEL Testing"
rm -f /etc/yum.repos.d/epel-testing.repo
sleep ${SLEEP}

echo "  .. Removing repository: EPEL Testing Modular"
rm -f /etc/yum.repos.d/epel-testing-modular.repo
sleep ${SLEEP}

echo "  .. Enabling repository: ELRepo"
if ! rpm -q elrepo-release > /dev/null 2>&1
then
  dnf install -y elrepo-release > /dev/null
fi
cp -f ${CWD}/dnf/elrepo.repo /etc/yum.repos.d/
sleep ${SLEEP}

# - Fetching missing packages from packages groups
# - ----------------------------------------------
echo
echo -e "${COLOR_WHITE}. Fetching missing packages from packages groups${COLOR_NC}"
echo "  .. This might take a moment..."
dnf -y group mark remove "Core"> /dev/null
dnf -y group install "Core" > /dev/null
echo "  .. 'Core' package group installed on the system"
sleep ${SLEEP}

dnf -y group mark remove "Base" > /dev/null 2>&1
dnf -y group install "Base" > /dev/null
echo "  .. 'Base' package group installed on the system"
sleep ${SLEEP}

# - Installing some additional packages -
# - ----------------------------------- -
echo
echo -e "${COLOR_WHITE}. Installing some additional packages${COLOR_NC}"
for PACKAGE in ${TOOLS}
do
  if ! rpm -q ${PACKAGE} > /dev/null 2>&1
  then
    echo "  .. Installing package: ${PACKAGE}"
    dnf install -y ${PACKAGE} > /dev/null 2>&1
    sleep ${SLEEP}
  fi
done
sleep ${SLEEP}

# - End
# - ---
echo
echo -e "${COLOR_WHITE}.: End :.${COLOR_NC}"
echo -e "  " ${COLOR_GREEN}${TEXT_BOLD}$'\u2714'${COLOR_NC}${TEXT_NORMAL} "Basic system tools successfully installed on the system!"
echo

exit 0
