#!/bin/bash
#
# - reset.sh
# - --------
#   Display a packages list which were added since the enhanced base system setup
#     . Previous generated packages list can be created thanks to the following command:
#     . # rpm -qa --queryformat "%{NAME}\n" | sort > $(pwd)/pkglists/base.txt

CWD=$(pwd)
TMP=${CWD}/tmp
REFERENCE_FILE=${CWD}/pkglists/base.txt
CURRENT_FILE=${TMP}/current.txt
PACKAGE_INFO=${TMP}/package_info.txt
MAX_TOTAL_PACKAGES_FOR_INFO=10
SLEEP=1

echo
echo "== Restore enhanced base system =="
echo

# - Check if REFERENCE_FILE exists. If not, no diff packages list can be done! Bye!
if [ ! -f ${REFERENCE_FILE} ]
then
  echo "  => ${REFERENCE_FILE} doesn't exist."
  echo "  => Can't make a packages list diff between now and when you have setup your enhanced base system"
  echo "  => Bye!"
  echo
  exit 0
fi

# - Get BASE_FILE
echo ". Let's process on packages list"
mkdir -p ${TMP}
sleep ${SLEEP}

# - Create current packages list in CURRENT_FILE
echo "  .. Create current packages list"
rpm -qa --queryformat '%{NAME}\n' | sort > ${CURRENT_FILE}
sleep ${SLEEP}

# - Create diff between REFERENCE_FILE & CURRENT_FILE
echo "  .. Create diff packages list (reference vs current)"
PACKAGE_LIST_DIFF=$(diff --old-line-format='' --unchanged-line-format='' ${REFERENCE_FILE} ${CURRENT_FILE})
sleep ${SLEEP}

# - How many packages to display?
CURRENT_PACKAGES_TOTAL=$(cat ${CURRENT_FILE} | wc -l)
REFERENCE_PACKAGES_TOTAL=$(cat ${REFERENCE_FILE} | wc -l)
DIFF_PACKAGES_TOTAL=`expr ${CURRENT_PACKAGES_TOTAL} - ${REFERENCE_PACKAGES_TOTAL}`

# - Display all new packages which were installed since reference time
echo
echo ". The followings ${DIFF_PACKAGES_TOTAL} packages were installed since your enhanced base system setup"
sleep ${SLEEP}

if [ ${DIFF_PACKAGES_TOTAL} -le ${MAX_TOTAL_PACKAGES_FOR_INFO} ]
then
  # - Few packages to display. Let's keep some extra informations
  for PACKAGE_NAME in ${PACKAGE_LIST_DIFF}
  do
    dnf info ${PACKAGE_NAME} > ${PACKAGE_INFO}

    PACKAGE_REPOSITORY=$(awk -F ": " '{print $2}' <<< $(grep 'From repo' ${PACKAGE_INFO}))
    PACKAGE_SUMMARY=$(awk -F ": " '{print $2}' <<< $(grep 'Summary' ${PACKAGE_INFO}))

    echo "  .. ${PACKAGE_NAME}  -  Repo: ${PACKAGE_REPOSITORY}  -  Summary: ${PACKAGE_SUMMARY}"
  done

else
  # - There is a lot of packages to display. Make it simple!
  for PACKAGE_NAME in ${PACKAGE_LIST_DIFF}
  do
    echo "  .. ${PACKAGE_NAME}"
  done
fi

# - End
# - ---
rm -rf ${TMP}
echo
