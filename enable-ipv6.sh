#!/bin/bash
#
# enable-ipv6.sh

COLOR_GREEN='\033[1;32m'
COLOR_RED='\033[0;31m'
COLOR_WHITE='\033[1;37m'
COLOR_NC='\033[0m'
TEXT_BOLD='\033[1m'
TEXT_NORMAL='\033[0m'
CWD=$(pwd)
SLEEP=1
SUPPORT_PRODUCT_EXPECTED="Rocky Linux"
SUPPORT_PRODUCT_VERSION_EXPECTED="9"

echo
echo -e "${COLOR_WHITE}.: ${SUPPORT_PRODUCT_EXPECTED} ${SUPPORT_PRODUCT_VERSION_EXPECTED} > Enabling IPv6 :.${COLOR_NC}"
echo

# - Check OS name & OS Version for process
# - --------------------------------------
source /etc/os-release
MAJOR_VERSION=$(awk -F"." '{print $1}' <<< $REDHAT_SUPPORT_PRODUCT_VERSION)
if [ "${REDHAT_SUPPORT_PRODUCT}" != "${SUPPORT_PRODUCT_EXPECTED}" ] || [ "${MAJOR_VERSION}" != "${SUPPORT_PRODUCT_VERSION_EXPECTED}" ]
then
  echo "=> A ${SUPPORT_PRODUCT_EXPECTED} ${SUPPORT_PRODUCT_VERSION_EXPECTED}.xx Operating System is expected! Bye!"
  echo
  exit 1
fi

# - Check that user is root or has superuser privileges
# - ---------------------------------------------------
if [ "${UID}" -ne 0 ]
then
  echo
  echo -e "${COLOR_RED}  => You must have root privileges. Use sudo or be root! Bye!${COLOR_NC}"
  echo

  exit 1
fi

# - Enabling IPv6
# - -------------
echo -e "${COLOR_WHITE}. Enabling IPv6${COLOR_NC}"
rm -f /etc/sysctl.d/disable-ipv6.conf
cp ${CWD}/sysctl.d/enable-ipv6.conf /etc/sysctl.d/
sysctl -p --load /etc/sysctl.d/enable-ipv6.conf > /dev/null
echo "  .. IPV6 is now enabled"

# - Reconfigure SSH
# - ---------------
echo
echo -e "${COLOR_WHITE}. Configuring SSH server for IPv6${COLOR_NC}"

if [ -f /etc/ssh/sshd_config ]
then
  sed -i -e 's/^AddressFamily inet/#AddressFamily any/g' /etc/ssh/sshd_config
  sed -i -e 's/^ListenAddress 0.0.0.0/#ListenAddress 0.0.0.0/g' /etc/ssh/sshd_config
  systemctl reload sshd
fi
echo "  .. SSH is now configured with IPv6"

# - End
# - ---
echo
echo -e "${COLOR_WHITE}.: End :.${COLOR_NC}"
echo -e "  " ${COLOR_GREEN}${TEXT_BOLD}$'\u2714'${COLOR_NC}${TEXT_NORMAL} "IPv6 is now enabled!"
echo

exit 0

